const express = require('express')
const config = require('config')
const app = express()


const { formatosAceitos, SerializadorErro } = require('./Serializador')

/** ERROS */
const NaoEncontrado = require('./erros/NaoEncontrado')
const CampoInvalido = require('./erros/CampoInvalido')
const DadosNaoFornecidos = require('./erros/DadosNaoFornecidos')
const MediaNaoSuportada = require('./erros/MediaTypeNaoSuportada')


const roteadorV2 = require('./routes/fornecedores/rotas.v2')
const roteador = require('./routes/fornecedores')


app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.use((req, res, next) => {

    let formatoRequisitado = req.header('Accept')

    if (formatosAceitos.indexOf(formatoRequisitado) === -1) {
        throw new MediaNaoSuportada(formatoRequisitado)
    }

    if (formatoRequisitado === '*/*') formatoRequisitado = 'application/json'
    res.setHeader('Content-Type', formatoRequisitado)
    next()
})

app.use((req, res, next) => {
    res.set('Access-Control-Allow-Origin', '*')
    next()
})

app.use('/api/v2/fornecedores/', roteadorV2)
app.use('/api/fornecedores', roteador)

app.use((erro, req, res, next) => {

    const serializador = new SerializadorErro(res.getHeader('Content-Type'))

    let status = 500

    if (erro instanceof NaoEncontrado) {
        status = 404
    } else if (erro instanceof MediaNaoSuportada) {
        status = 415
    } else if (erro instanceof CampoInvalido || erro instanceof DadosNaoFornecidos) {
        status = 400
    }

    const response = { id: erro.idErro, messagem: erro.message }
    res.status(status).send(serializador.serializar(response))
})

app.listen(config.get('api.port'), () => console.log("Funcionando"))