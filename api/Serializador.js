const MediaTypeNaoSuportada = require("./erros/MediaTypeNaoSuportada")

const jsontoxml = require('jsontoxml')

class Serializador {

    json(dados) {
        return JSON.stringify(dados)
    }

    xml(dados) {

        let tag = this.tagSingular

        if (Array.isArray(dados)) {
            tag = this.tagPlural
            dados = dados.map(dado => {
                return { [this.tagSingular]: dado }
            })
        }

        return jsontoxml({ [tag]: dados })
    }


    serializar(dados) {

        const dadosFiltrados = this.filtrar(dados)

        if (this.contentType === 'application/json' || this.contentType === '*/*') {
            return this.json(dadosFiltrados)
        } else if (this.contentType === 'application/xml') {
            return this.xml(dadosFiltrados)
        }

        throw new MediaTypeNaoSuportada(this.contentType)
    }


    filtraObjeto(dados) {

        if (!dados) return {}

        const novoObjeto = {}

        this.camposPublicos.forEach(campo => {            
            if (dados.hasOwnProperty(campo)) {
                novoObjeto[campo] = dados[campo]
            }
        })

        return novoObjeto

    }

    filtrar(dados) {
        if (Array.isArray(dados)) {            
            dados = dados.map(dado => this.filtraObjeto(dado))
        } else {
            dados = this.filtraObjeto(dados)
        }

        return dados
    }

}


class SerializadorProduto extends Serializador {
    constructor(contentType, camposExtras = []) {
        super()

        this.tagSingular = 'produto'
        this.tagPlural = 'produtos'
        this.contentType = contentType

        this.camposPublicos = [
            'id',
            'titulo',
            'preco',                      
            ...camposExtras
        ]
    }
}

class SerializadorFornecedor extends Serializador {
    constructor(contentType, camposExtras = []) {
        super()

        this.tagSingular = 'fornecedor'
        this.tagPlural = 'fornecedores'
        this.contentType = contentType

        this.camposPublicos = [
            'id',            
            'categoria',
            ...camposExtras
        ]
    }
}


class SerializadorErro extends Serializador {
    constructor(contentType, camposExtras = []) {
        super()
        this.tagSingular = 'error'
        this.tagPlural = 'errors'
        this.contentType = contentType
        this.camposPublicos = [
            'id',
            'messagem',
            ...camposExtras
        ]

    }
}


module.exports = {

    Serializador,

    SerializadorFornecedor,
    SerializadorProduto,

    SerializadorErro,

    formatosAceitos: [
        '*/*',
        'application/json',
        'application/xml'
    ]
}