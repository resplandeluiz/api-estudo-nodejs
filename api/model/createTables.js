const modelos = [
    require('../routes/fornecedores/tabelaFornecedor'),
    require('../routes/produtos/tabelaProduto')
]

async function criarTabelas() {
    modelos.map(modelo => {
        
        let message = `Tabela ${modelo.options.tableName} criada com sucesso`
        
        modelo.sync()
        .then(() => console.log(message))
        .catch(e => console.log(e))
        
    })
}

criarTabelas()

