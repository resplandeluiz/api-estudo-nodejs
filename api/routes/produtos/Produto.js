const CampoInvalido = require('../../erros/CampoInvalido')
const DadosNaoFornecidos = require('../../erros/DadosNaoFornecidos')
const NaoEncontrado = require('../../erros/NaoEncontrado')
const bdProduto = require('./bdProduto')

class Produto {

    constructor({ id, titulo, preco, estoque = 0, fornecedor, createdAt, updatedAt, version }) {

        this.id = id
        this.titulo = titulo
        this.preco = preco
        this.estoque = estoque
        this.fornecedor = fornecedor
        this.createdAt = createdAt
        this.updatedAt = updatedAt
        this.version = version

    }


    validar() {

        const { titulo, preco } = this

        if (typeof titulo !== 'string' || titulo.length <= 0) {
            throw new CampoInvalido('título')
        } else if (typeof preco !== 'number' || preco <= 0) {
            throw new DadosNaoFornecidos('preco')
        }

    }

    async adicionar() {

        this.validar()

        const { titulo, preco, estoque, fornecedor } = this

        const produto = await bdProduto.adicionar({ titulo, preco, estoque, fornecedor })

        this.id = produto.id
        this.createdAt = produto.createdAt
        this.updatedAt = produto.updatedAt
        this.version = produto.version

    }

    async listarPorId() {
        try {
            const { id, fornecedor } = this
            const { titulo, preco, estoque, createdAt, updatedAt, version } = await bdProduto.listarPorId(id, fornecedor)

            this.titulo = titulo
            this.preco = preco
            this.estoque = estoque
            this.createdAt = createdAt
            this.updatedAt = updatedAt
            this.version = version
        } catch (e) {
            throw new NaoEncontrado('Produto')
        }

    }


    async deletar() {

        const { id, fornecedor } = this

        await bdProduto.deletar(id, fornecedor)

    }


    async atualizar() {
        try {

            const { id, titulo, preco, estoque, fornecedor } = this

            const existeProduto = await bdProduto.listarPorId(id, fornecedor)

            if (!existeProduto) throw new NaoEncontrado('Produto')

            let dadosParaAtualizar = {}

            if (typeof titulo === 'string' && titulo.length > 0)
                dadosParaAtualizar.titulo = titulo

            if (typeof preco === 'number' && preco >= 0)
                dadosParaAtualizar.preco = preco

            if (typeof estoque === 'number' && estoque >= 0)
                dadosParaAtualizar.estoque = estoque

            if (Object.keys(dadosParaAtualizar).length === 0) throw new DadosNaoFornecidos()
            await bdProduto.atualizar(id, dadosParaAtualizar)

        } catch (e) {
            throw new Error(e.message)
        }
    }


    async dimiuirEstoque() {

        const { id, fornecedor } = this

        return bdProduto.subtrair(id, fornecedor, 'estoque', this.estoque)
    }


}


module.exports = Produto