
const Modelo = require('./tabelaProduto')
const Sequelize = require('../../model')

module.exports = {

    listar: (idFornecedor) => Modelo.findAll({ where: { fornecedor: idFornecedor }, raw: true }),

    adicionar: (produto) => Modelo.create(produto),

    deletar: (id, fornecedor) => Modelo.destroy({ where: { id, fornecedor } }),

    listarPorId: (id, fornecedor) => Modelo.findOne({ where: { id, fornecedor } }),

    atualizar: (id, dados) => Modelo.update(dados, { where: { id } }),

    subtrair: (id, fornecedor, campo, quantidade) => Sequelize.transaction(async () => {

        const produto = await Modelo.findOne({ where: { id, fornecedor } })
        produto[campo] = quantidade

        await produto.save()

        return produto
    })

}