const roteador = require('express').Router({ mergeParams: true })
const bdProduto = require('./bdProduto')

const Produto = require('./Produto')
const SerializadorProduto = require('../../Serializador').SerializadorProduto

roteador.options('/', (req, res) => {
    res.set('Access-Control-Allow-Methods', 'GET, PUT')
    res.set('Access-Control-Allow-Headers', 'Content-Type')
    res.status(204).end()
})

roteador.options('/:idProduto', (req, res) => {
    res.set('Access-Control-Allow-Methods', 'GET, HEAD, POST, PUT, DELETE')
    res.set('Access-Control-Allow-Headers', 'Content-Type')
    res.status(204).end()
})


roteador.get('/', async (req, res) => {
    const serializador = new SerializadorProduto(res.getHeader("Content-Type"))
    const { idFornecedor } = req.params
    let produtos = await bdProduto.listar(idFornecedor)
    res.send(serializador.serializar(produtos))

})

roteador.get('/:idProduto', async (req, res, next) => {

    try {
        const serializador = new SerializadorProduto(
            res.getHeader("Content-Type"),
            ['estoque', 'fornecedor', 'createdAt', 'updatedAt', 'version'])

        const { idFornecedor: fornecedor, idProduto: id } = req.params

        const produto = new Produto({ fornecedor, id })
        await produto.listarPorId()

        res.set('Etag', produto.version)
        res.set('X-Powered-By', 'DOG')
        const timestamp = (new Date(produto.updatedAt)).getTime()
        res.set('Last-Modified', timestamp)

        res.send(serializador.serializar(produto))

    } catch (e) {
        next(e)
    }


})

roteador.head('/:idProduto', async (req, res, next) => {
    try {

        const { idFornecedor: fornecedor, idProduto: id } = req.params

        const produto = new Produto({ fornecedor, id })
        await produto.listarPorId()

        res.set('Etag', produto.version)
        res.set('X-Powered-By', 'DOG')
        const timestamp = (new Date(produto.updatedAt)).getTime()
        res.set('Last-Modified', timestamp)
        res.end()

    } catch (e) {
        next(e)
    }
})

roteador.post('/', async (req, res, next) => {
    try {
        const serializador = new SerializadorProduto(
            res.getHeader("Content-Type"))
        const { idFornecedor: fornecedor } = req.params
        const requestBody = req.body

        const dados = { fornecedor, ...requestBody }
        const produto = new Produto(dados)
        await produto.adicionar()

        res.set('Etag', produto.version)

        const timestamp = (new Date(produto.updatedAt)).getTime()
        res.set('Last-Modified', timestamp)
        res.set('Location', `/api/fornecedores/${produto.fornecedor}/produtos/${produto.id}`)

        res.status(201).send(serializador.serializar(produto))

    } catch (e) {
        next(e)
    }

})


roteador.delete('/:idProduto', async (req, res) => {

    const { idFornecedor: fornecedor, idProduto: id } = req.params
    const produto = new Produto({ id, fornecedor })
    await produto.deletar()

    res.status(204).end()

})


roteador.put('/:idProduto', async (req, res, next) => {
    try {

        const requestBody = req.body
        const { idFornecedor: fornecedor, idProduto: id } = req.params

        const dados = { id, fornecedor, ...requestBody }
        const produto = new Produto(dados)

        await produto.atualizar()
        await produto.listarPorId()

        res.set('Etag', produto.version)

        const timestamp = (new Date(produto.updatedAt)).getTime()
        res.set('Last-Modified', timestamp)

        res.status(204).end()

    } catch (e) {
        next(e)
    }
})

roteador.options('/:idProduto/diminuirEstoque', (req, res) => {
    res.set('Access-Control-Allow-Methods', 'POST')
    res.set('Access-Control-Allow-Headers', 'Content-Type')
    res.status(204).end()
})

roteador.post('/:idProduto/diminuirEstoque', async (req, res, next) => {

    try {

        const { idFornecedor: fornecedor, idProduto: id } = req.params
        const { quantidade } = req.body

        const produto = new Produto({ id, fornecedor })

        await produto.listarPorId()

        produto.estoque -= quantidade
        if (produto.estoque < 0) produto.estoque = 0

        await produto.dimiuirEstoque()
        await produto.listarPorId()

        res.set('Etag', produto.version)

        const timestamp = (new Date(produto.updatedAt)).getTime()
        res.set('Last-Modified', timestamp)

        res.status(204).end()


    } catch (e) {
        next(e.message)
    }

})


module.exports = roteador