const Sequelize = require('sequelize')
const bdInstancia = require('../../model')

const columns = {
    titulo: {
        type: Sequelize.STRING,
        allowNull: false
    },
    preco: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    estoque: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultVale: 0
    },
    fornecedor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: require('../fornecedores/tabelaFornecedor'),
            key: 'id'
        }
    }
}

const options = {
    freezeTableName: true,
    tableName: 'produtos',
    timestamps: true,
    version: 'version'
}

module.exports = bdInstancia.define('produto', columns, options)