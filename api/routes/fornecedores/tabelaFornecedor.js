const Sequelize = require('sequelize')
const bdInstancia = require('../../model')

const columns = {
    empresa: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    categoria: {
        type: Sequelize.ENUM("ração", 'brinquedos'),
        allowNull: false
    }
}

const options = {
    freezeTableName: true,
    tableName: 'fornecedores',
    timestamps: true,
    version: 'version'
}


module.exports = bdInstancia.define('fornecedor', columns, options)