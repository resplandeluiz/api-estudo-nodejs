const Modelo = require('./tabelaFornecedor')

module.exports = {

    listar: () => Modelo.findAll({ raw: true }),

    adicionar: (fornecedor) => Modelo.create(fornecedor),

    listarPorId: (id) => Modelo.findOne({ where: { id }, raw: true }),

    atualizar: (id, dados) => Modelo.update(dados, { where: { id } }),

    deletar: (id) => Modelo.destroy({ where: { id } })

}