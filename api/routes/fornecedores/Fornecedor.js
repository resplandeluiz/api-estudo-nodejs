
const bdFornecedor = require('./bdFornecedores')
const NaoEncontrado = require('../../erros/NaoEncontrado')
const CampoInvalido = require('../../erros/CampoInvalido')
const DadosNaoFornecidos = require('../../erros/DadosNaoFornecidos')

class Fornecedor {

    constructor({ id, empresa, email, categoria, createdAt, updatedAt, version }) {

        this.id = id
        this.empresa = empresa
        this.email = email
        this.categoria = categoria
        this.createdAt = createdAt
        this.updatedAt = updatedAt
        this.version = version

    }


    async adicionar() {

        this.validar()

        const resultado = await bdFornecedor.adicionar({
            empresa: this.empresa,
            email: this.email,
            categoria: this.categoria
        })

        this.id = resultado.id
        this.createdAt = resultado.createdAt
        this.updatedAt = resultado.updatedAt
        this.version = resultado.version

    }


    validar() {
        const campos = ['empresa', 'email', 'categoria']

        campos.forEach(campo => {

            const valor = this[campo]
            
            if (typeof valor !== 'string' || valor.length <= 0) {
                throw new CampoInvalido(campo)
            }

        })
    }

    async atualizar() {

        const existeFornecedor = await bdFornecedor.listarPorId(this.id)

        if (!existeFornecedor) throw new NaoEncontrado('Fornecedor')

        const campos = ['empresa', 'email', 'categoria']
        const dadosParaAtualizar = {}

        campos.forEach(campo => {

            const valor = this[campo]

            if (typeof valor === 'string' && valor.length > 0) {
                dadosParaAtualizar[campo] = valor
            }

        })

        if (Object.keys(dadosParaAtualizar).length === 0)
            throw new DadosNaoFornecidos()

        await bdFornecedor.atualizar(this.id, dadosParaAtualizar)
        const fornecedorAtualizado = await bdFornecedor.listarPorId(this.id)

        return fornecedorAtualizado

    }

}

module.exports = Fornecedor