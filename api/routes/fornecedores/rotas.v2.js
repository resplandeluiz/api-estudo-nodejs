const roteador = require('express').Router()
const bdFornecedor = require('./bdFornecedores')


const SerializadorFornecedor = require('../../Serializador').SerializadorFornecedor


roteador.options('/', (req, res) => {
    res.set('Access-Control-Allow-Methods', 'GET')
    res.set('Access-Control-Allow-Headers', 'Content-Type')
    res.status(204).end()
})

roteador.get('/', async (req, res, next) => {
    try {

        const fornecedores = await bdFornecedor.listar()
        const serializador = new SerializadorFornecedor(res.getHeader("Content-Type"))
        res.send(serializador.serializar(fornecedores))

    } catch (e) { next(e) }
})

module.exports = roteador