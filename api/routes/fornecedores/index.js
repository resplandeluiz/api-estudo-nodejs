const roteador = require('express').Router()
const bdFornecedor = require('./bdFornecedores')


const NaoEncontrado = require('../../erros/NaoEncontrado')
const Fornecedor = require('./Fornecedor')
const SerializadorFornecedor = require('../../Serializador').SerializadorFornecedor


roteador.options('/', (req, res) => {
    res.set('Access-Control-Allow-Methods', 'GET, POST')
    res.set('Access-Control-Allow-Headers', 'Content-Type')
    res.status(204).end()
})

roteador.options('/:idFornecedor', (req, res) => {
    res.set('Access-Control-Allow-Methods', 'GET, PUT, DELETE')
    res.set('Access-Control-Allow-Headers', 'Content-Type')
    res.status(204).end()
})

roteador.get('/', async (req, res, next) => {
    try {

        const fornecedores = await bdFornecedor.listar()
        const serializador = new SerializadorFornecedor(res.getHeader("Content-Type"), ['empresa'])
        res.send(serializador.serializar(fornecedores))

    } catch (e) { next(e) }
})

roteador.get('/:idFornecedor', async (req, res, next) => {

    const id = req.params.idFornecedor
    const response = await bdFornecedor.listarPorId(id)
    const serializador = new SerializadorFornecedor(res.getHeader("Content-Type"), ['empresa', 'email', 'createdAt', 'updatedAt', 'version'])

    if (response === null)
        next(new NaoEncontrado('Fornecedor'))
    else
        res.send(serializador.serializar(response))
})

roteador.put('/:idFornecedor', async (req, res, next) => {
    try {
        const id = req.params.idFornecedor
        const data = req.body
        const serializador = new SerializadorFornecedor(res.getHeader("Content-Type"))
        const dados = Object.assign({}, data, { id })
        const fornecedor = new Fornecedor(dados)
        const response = await fornecedor.atualizar()


        res.send(serializador.serializar(response))

    } catch (e) {
        next(e)
    }
})

roteador.delete('/:idFornecedor', async (req, res, next) => {
    try {

        const serializador = new SerializadorFornecedor(res.getHeader("Content-Type"))
        const id = req.params.idFornecedor
        const fornecedor = await bdFornecedor.listarPorId(id)

        if (fornecedor === null) throw new NaoEncontrado('Fornecedor')

        await bdFornecedor.deletar(id)
        res.send(serializador.serializar({}))

    } catch (e) {
        next(e)
    }
})

roteador.post('/', async (req, res, next) => {

    try {
        const serializador = new SerializadorFornecedor(res.getHeader("Content-Type"))

        const fornecedor = new Fornecedor(req.body)
        let resultado = await fornecedor.adicionar()
        res.status(201).send(serializador.serializar(resultado))

    } catch (e) {
        next(e)
    }

})


const roteadorProdutos = require('../produtos')

const verificarFonecedor = async (req, res, next) => {
    const { idFornecedor } = req.params
    let response = await bdFornecedor.listarPorId(idFornecedor)

    if (response === null)
        next(new NaoEncontrado('Fornecedor'))
    else
        next()
}


roteador.use('/:idFornecedor/produtos', verificarFonecedor, roteadorProdutos)

module.exports = roteador