
class MediaNaoSuportada extends Error {

    constructor(contentType) {

        const message = `Média [${contentType}] não suportada, apenas JSON.`

        super(message)

        this.name = 'MediaNaoSuportada'
        this.idErro = 3

    }
}

module.exports = MediaNaoSuportada