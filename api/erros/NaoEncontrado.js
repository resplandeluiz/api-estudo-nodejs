
class NaoEncontrado extends Error {

    constructor(nome) {

        const message = `${nome} não foi encontrado`

        super(message)
        
        this.name = 'NaoEncontrado'
        this.idErro = 0

    }
}

module.exports = NaoEncontrado